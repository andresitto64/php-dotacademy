<?php

// Genera un array di 20 numeri casuali senza duplicati compresi tra 1 e 100
$numbers = [];
for ($i = 0; $i < 20; $i++) {
    $num = rand(1, 100);
    while (in_array($num, $numbers)) {
        $num = rand(1, 100);
    }
    $numbers[] = $num;
}

// Stampa l'array
echo "Array: ";
foreach ($numbers as $num) {
    echo $num ;
}
echo "\n";

// Calcola la somma dei numeri pari
$evenSum = 0;
foreach ($numbers as $num) {
    if ($num % 2 == 0) {
        $evenSum += $num;
    }
}
echo "Somma numeri: " . $evenSum . "\n";

// Verifica se il numero 33 è presente nell'array
if (in_array(33, $numbers)) {
    echo "Il numero 33 è presente nell'array<br>";
} else {
    echo "Il numero 33 non è presente nell'array<br>";
}

// Trova la posizione del primo numero dispari
$indice = -1;
for ($i = 0; $i < 20; $i++) {
    if ($numbers[$i] % 2 != 0) {
        $indice = $i;
        break;
    }
}
if ($indice != -1) {
    echo "Primo numero dispari: " . $numbers[$indice] . " (posizione " . ($indice + 1) . ")\n";
} else {
    echo "Non ci sono numeri dispari nell'array\n";
}

?>
